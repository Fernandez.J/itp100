def to_base3(num):
    """
    Convert a decimal integer into a string representation of its base3 digits.
    >>> to_base3(10)
    '101'
    >>> to_base3(12)
    '110'
    >>> to_base3(0)
    '0'
    >>> to_base3(1)
    '1'
    >>> to_base3(6)
    '20'
    >>> to_base3(-5)
    ''
    >>> to_base3("dog")
    'potato'
    >>> to_base3(105)
    '10220'
    """ 
    new = ''
    if type(num) == int:
        if num == 0:
            return '0'
        while num > 0:
            a = num//3
            b = num % 3
            new = str(b) + new
            num = a
    else:
        return 'potato'
    return new

if __name__=='__main__':
    import doctest
    doctest.testmod()

