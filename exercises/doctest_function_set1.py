def only_evens(nums):
    """
      >>> only_evens([3, 8, 5, 4, 12, 7, 2])
      [8, 4, 12, 2]
      >>> my_nums = [4, 7, 19, 22, 42]
      >>> only_evens(my_nums)
      [4, 22, 42]
      >>> my_nums
      [4, 7, 19, 22, 42]
    """

    new_nums = []
    for i in nums:
        if i % 2 == 0:
            new_nums.append(i)
    return new_nums

def num_even_digits(n):
    """
      >>> num_even_digits(123456)
      3
      >>> num_even_digits(2468)
      4
      >>> num_even_digits(1357)
      0
      >>> num_even_digits(2)
      1
      >>> num_even_digits(20)
      2
    """
    evens = 0
    for i in str(n):
        if int(i) % 2 == 0:
            evens += 1
    return evens

def sum_of_squares_of_digits(n):
    """
      >>> sum_of_squares_of_digits(1)
      1
      >>> sum_of_squares_of_digits(9)
      81
      >>> sum_of_squares_of_digits(11)
      2
      >>> sum_of_squares_of_digits(121)
      6
      >>> sum_of_squares_of_digits(987)
      194
    """
    the_sum = 0
    for i in str(n):
        the_sum += int(i) ** 2
    return the_sum

def lots_of_letters(word):
    """
      >>> lots_of_letters('Lidia')
      'Liidddiiiiaaaaa'
      >>> lots_of_letters('Python')
      'Pyyttthhhhooooonnnnnn'
      >>> lots_of_letters('')
      ''
      >>> lots_of_letters('1')
      '1'
    """
    l = 1
    new_string = ''
    for i in word:
        new_string += i * l
        l += 1
    return new_string

def gcf(m, n):
    """
      >>> gcf(10, 25)
      5
      >>> gcf(8, 12)
      4
      >>> gcf(5, 12)
      1
      >>> gcf(24, 12)
      12
    """
    f = m * n
    while not (m % f == 0 and n % f == 0):
        f -= 1
    return f

def is_prime(n):
    """
      >>> is_prime(2)
      True
      >>> is_prime(4)
      False
      >>> is_prime(13)
      True
    """
    if n <= 1:
        return False
    f = n - 1
    while f > 1:
        if n % f == 0:
            return False
        f -= 1
    return True


if __name__ == '__main__':
    import doctest
    doctest.testmod()

