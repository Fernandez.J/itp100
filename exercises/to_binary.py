def to_binary(num):
    """
    Convert a decimal integer into a string representation of its binary (base2) digits.
    >>> to_binary(10)
    '1010'
    >>> to_binary(12)
    '1100'
    >>> to_binary(1000)
    '1111101000'
    >>> to_binary(5)
    '101'
    >>> to_binary(0)
    '0'
    >>> to_binary(1)
    '1'
    >>> to_binary(2)
    '10'
    >>> to_binary(-5)
    ''
    >>> to_binary("dog")
    'potato'
    >>> to_binary(101010)
    '11000101010010010'
    >>> to_binary(503)
    '111110111'
    """ 
    new = ''
    if type(num) == int:
        if num == 0:
            return '0'
        while num > 0:
            a = num//2
            b = num % 2
            new = str(b) + new
            num = a
    else:
        return 'potato'
    return new

if __name__=='__main__':
    import doctest
    doctest.testmod()

