def to_base(num, base):
    """
    Convert a decimal integer into a string representation of the digits representing the number in the base (between 2 and 10) provided.
    >>> to_base(10, 3)
    '101'
    >>> to_base(11, 2)
    '1011'
    >>> to_base(0, 3)
    '0'
    >>> to_base(1, 6)
    '1'
    >>> to_base(10, 6)
    '14'
    >>> to_base(-5, 4)
    ''
    >>> to_base(105, 3)
    '10220'
    >>> to_base(15, 16)
    'F'
    >>> to_base(200, 16)
    'C8'
    >>> to_base(4, 64)
    'E'
    >>> to_base("dog", 9)
    'potato'
    >>> to_base(100, 64)
    'Bk'
    >>> to_base(903, 64)
    'OH'
    """
    digits = '0123456789ABCDEF'
    base64digits = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/'
    new = ''
    if num == 0:
        return '0'
    if type(num) != int:
        return 'potato'
    if base != 64:
        while num > 0:
            a = num//base
            b = digits[num % base]
            new = str(b) + new
            num = a
    else:
        while num:
            new = base64digits[num % base] + new
            num //= base
    return new

if __name__=='__main__':
    import doctest
    doctest.testmod()


