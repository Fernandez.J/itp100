#A very helpful resource for working with binary data in Python is:
#https://www.devdungeon.com/content/working-binary-data-python

#Now that we can convert back and forth between 3 bytes and 
#4 Base64 digits, our next task is to open a file containing bytes, read in
#3 bytes at a time, convert them into 4 Base64 digits (characters), and write
#them to another open text file.

def base64encode(three_bytes):
    """
      >>> base64encode(b'\\x5A\\x2B\\xE6')
      'Wivm'
      >>> base64encode(b'\\x49\\x33\\x8F')
      'STOP'
      >>> base64encode(b'\\xFF\\xFF\\xFF')
      '////'
      >>> base64encode(b'\\x00\\x00\\x00')
      'AAAA'
    """
    digits = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/'

    try:
        # turn the three bytes into ints
        b1, b2, b3 = three_bytes[0], three_bytes[1], three_bytes[2]
        # get first 6 bits of b1 
        index1 = b1 >> 2
        # join last 2 bits of b1 shifted left 4 with first 4 bits of b2
        index2 = (b1 & 3) << 4 | b2 >> 4
        # join last 4 bits of b2 shifted left 2 with first 2 bits of b3
        index3 = (b2 & 15) << 2 | (b3 & 192) >> 6
        # get last 6 bits of b3
        index4 = b3 & 63
    except (AttributeError, TypeError):
        raise AssertionError('Input should be 3 bytes')

    return f'{digits[index1]}{digits[index2]}{digits[index3]}{digits[index4]}'



def decode_base64(four_chars):
    """
        >>> base64decode('STOP')
        b'I3\\x8f'
    """
    digits = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/'
    ints = [digits.index(ch) for ch in four_chars]
    b1 = (ints[0] << 2) | ((ints[1] & 48) >> 4)
    b2 = (ints[1] & 15) << 4 | ints[2] >> 2
    b3 = ((ints[2] & 3 )<< 6) | (ints[3])

    return bytes([b1, b2, b3])

def encode_file(input_filename, output_filename):
    infile = open(input_filename, 'rb')
    outfile = open(output_filename, 'w')
    input_bytes = infile.read(3)
    while True:
    #change n to numer of ch or dig in file
        #print(input_bytes)
        encoded_bytes = base64encode(input_bytes)
        #print(encoded_bytes)
        outfile.write(encoded_bytes) 
        input_bytes = infile.read(3)
        if (len(input_bytes) < 3): 
            break

#def decode_file (f)

#print( encode_base64(int.from_bytes(b'ab', "little")))

encode_file("input_test.bin", "output_file.txt")







if __name__ == '__main__':
    import doctest
    doctest.testmod()

