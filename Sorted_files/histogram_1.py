import math

file = open('alice_in_wonderland.txt')
text = file.read()
file.close()
coin = [0]*128

for i in text:
    coin[ord(i)] += 1


print('ALICE IN WONDERLAND HISTOGRAM!')
for p in range(32, 128):
    print(str(chr(p)) + ' ' + str(coin[p]) + ' ' + ('█' * math.ceil(coin[p] / 250)))

