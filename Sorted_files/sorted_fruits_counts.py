file = open('unsorted_fruits_with_repeats.txt', 'r')
unsorted = file.readlines()
file.close()
sorted1 = sorted(unsorted)

pin = 0
names = [' ']
counter = [0]
for i in range(1, len(sorted1)):
    if sorted1[i - 1] == sorted1[i]:
        counter[pin] += 1
    else:
        counter.append(0)
        pin += 1
        counter[pin] = 1
        names.append(sorted1[i])

stringsorted = ''
inc = 0
for i in range(pin):
    if inc - 1:
        stringsorted += str(counter[inc]) + ' '
    stringsorted += names[i]
    inc += 1

file1 = open('sorted_fruits_counts.txt', 'w')
file1.write(stringsorted)
file1.close()


