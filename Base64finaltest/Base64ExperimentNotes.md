*EXPERIMENT*

We uncompressed the folder containing the encoding and decoding functions.
We encoded the kitten file using:

> python3 base64encode kitten.png

This made a text file called kitten__png.b64txt.
We decoded it.

> python3 base64decode kitten__png.b64txt

The decoding process took longer.
We opened the resulting image and found it to be identical to the original file.

