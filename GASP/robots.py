from gasp import *

scale = 10

begin_graphics()

player_x = random_between(0, 64) * scale
player_y = random_between(0, 46) * scale
c = Circle((player_x, player_y), 5, filled=True)

robot_x = random_between(0, 64) * scale
robot_y = random_between(0, 46) * scale
r = Box((robot_x, robot_y), 10, 10, filled=True)

robot_x2 = random_between(0, 64) * scale
robot_y2 = random_between(0, 46) * scale
r2 = Box((robot_x2, robot_y2), 10, 10, filled=True)

robot_x3 = random_between(0, 64) * scale
robot_y3 = random_between(0, 46) * scale
r3 = Box((robot_x3, robot_y3), 10, 10, filled=True)

robot_x4 = random_between(0, 64) * scale
robot_y4 = random_between(0, 46) * scale
r4 = Box((robot_x4, robot_y4), 10, 10, filled=True)

robot_x5 = random_between(0, 64) * scale
robot_y5 = random_between(0, 46) * scale
r5 = Box((robot_x5, robot_y5), 10, 10, filled=True)

robot_x6 = random_between(0, 64) * scale
robot_y6 = random_between(0, 46) * scale
r6 = Box((robot_x6, robot_y6), 10, 10, filled=True)


instructions = Text("You are the circle. Do not let the Robot Square touch you!", (50, 400), size=20)

finished = False
moves = 0

while not finished:
    key = update_when('key_pressed')

    if moves == 0:
        remove_from_screen(instructions)
        
    if key == 'up':
        player_y += scale
    elif key == 'down':
        player_y -= scale
    elif key == 'right':
        player_x += scale
    elif key == 'left':
        player_x -= scale
    move_to(c, (player_x, player_y))
    
    pin = random_between(0,1)

    if pin == 1 and player_y != robot_y:
        if robot_y < player_y:
            robot_y += scale
        elif robot_y > player_y:
            robot_y -= scale
    elif pin == 0 and player_x != robot_x:
        if robot_x < player_x:
            robot_x += scale
        elif robot_x > player_x:
            robot_x -= scale
    move_to(r, (robot_x, robot_y))
    
    if pin == 1 and player_y != robot_y2:
        if robot_y2 < player_y:
            robot_y2 += scale
        elif robot_y2 > player_y:
            robot_y2 -= scale
    elif pin == 0 and player_x != robot_x2:
        if robot_x2 < player_x:
            robot_x2 += scale
        elif robot_x2 > player_x:
            robot_x2 -= scale
    move_to(r2, (robot_x2, robot_y2))
    
    if pin == 1 and player_y != robot_y3:
        if robot_y3 < player_y:
            robot_y3 += scale
        elif robot_y3 > player_y:
            robot_y3 -= scale
    elif pin == 0 and player_x != robot_x3:
        if robot_x3 < player_x:
            robot_x3 += scale
        elif robot_x3 > player_x:
            robot_x3 -= scale
    move_to(r3, (robot_x3, robot_y3))
    
    if pin == 1 and player_y != robot_y4:
        if robot_y4 < player_y:
            robot_y4 += scale
        elif robot_y4 > player_y:
            robot_y4 -= scale
    elif pin == 0 and player_x != robot_x4:
        if robot_x4 < player_x:
            robot_x4 += scale
        elif robot_x4 > player_x:
            robot_x4 -= scale
    move_to(r4, (robot_x4, robot_y4))
    
    if pin == 1 and player_y != robot_y5:
        if robot_y5 < player_y:
            robot_y5 += scale
        elif robot_y5 > player_y:
            robot_y5 -= scale
    elif pin == 0 and player_x != robot_x5:
        if robot_x5 < player_x:
            robot_x5 += scale
        elif robot_x5 > player_x:
            robot_x5 -= scale
    move_to(r5, (robot_x5, robot_y5))
    
    if pin == 1 and player_y != robot_y6:
        if robot_y6 < player_y:
            robot_y6 += scale
        elif robot_y6 > player_y:
            robot_y6 -= scale
    elif pin == 0 and player_x != robot_x6:
        if robot_x6 < player_x:
            robot_x6 += scale
        elif robot_x6 > player_x:
            robot_x6 -= scale
    move_to(r6, (robot_x6, robot_y6))

    player_x = player_x % 640
    player_y %= 480
    robot_x %= 640
    robot_y %= 480
    moves += 1
    
    player_x = player_x % 640
    player_y %= 480
    robot_x2 %= 640
    robot_y2 %= 480
    moves += 1
    
    player_x = player_x % 640
    player_y %= 480
    robot_x3 %= 640
    robot_y3 %= 480
    moves += 1
    
    player_x = player_x % 640
    player_y %= 480
    robot_x4 %= 640
    robot_y4 %= 480
    moves += 1
    
    player_x = player_x % 640
    player_y %= 480
    robot_x5 %= 640
    robot_y5 %= 480
    moves += 1
    
    player_x = player_x % 640
    player_y %= 480
    robot_x6 %= 640
    robot_y6 %= 480
    moves += 1

    if robot_x == player_x and robot_y == player_y:
        finished = True
        
    if robot_x2 == player_x and robot_y2 == player_y:
        finished = True
        
    if robot_x3 == player_x and robot_y3 == player_y:
        finished = True
        
    if robot_x4 == player_x and robot_y4 == player_y:
        finished = True
        
    if robot_x5 == player_x and robot_y5 == player_y:
        finished = True
        
    if robot_x6 == player_x and robot_y6 == player_y:
        finished = True

end_text = Text(f"GAME OVER. You got trapped. {moves} moves. Press 'e' to exit.", (50, 50), size=15)

while True:
    key = update_when('key_pressed')
    if key == 'e':
        break

end_graphics()

