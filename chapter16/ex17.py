def sumPos(theList):
    sum = 0
    for item in theList:
        if item >= 0:
            sum = sum + item
    return sum

print(sumPos([-3, 2, -8, 5, -20, -33, 15]))
