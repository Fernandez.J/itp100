def gradeAverage(aList):
    sum = 0
    for num in aList:
        sum = sum + num
    average =  sum / len(aList)
    return average

aList = [99, 100, 74, 63, 100, 100]
print(gradeAverage(aList))
