def aFunction(aList):
    posSum = 0
    negSum = 0
    for x in aList:
        if x >= 0:
            posSum = posSum + x
        else:
            negSum = negSum + x
    avg = (posSum + (negSum * -1))/2
    return avg

nums = [-3,-1,2,4]
print(aFunction(nums))
